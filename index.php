<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Google reCAPTCHA v3</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <script src="https://www.google.com/recaptcha/api.js?render=6LeaxrUUAAAAAOGrDHu0070G-QgYDWiFAXijwQZa"></script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('6LeaxrUUAAAAAOGrDHu0070G-QgYDWiFAXijwQZa', { action: 'contact' }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>
</head>

<body>
    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-half">

                    <?php // Check if form was submitted:
                    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['recaptcha_response'])) {

                        // Build POST request:
                        $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
                        $recaptcha_secret = '6LeaxrUUAAAAAHw4SAy4_8uJKyGDzi3kKTFvg_Al';
                        $recaptcha_response = $_POST['recaptcha_response'];
    $recaptcha_response = $_POST['recaptcha_response'];
                        // Make and decode POST request:
                        var_dump($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
                        $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
                        // $recaptcha = json_decode($recaptcha);
var_dump( $recaptcha);
                        // Take action based on the score returned:
                        // if ($recaptcha->score >= 0.5) {
                        //     // Verified - send email
                        // } else {
                        //     // Not verified - show form error
                        // }
                    } ?>

                    <form method="POST">

                        <h1 class="title">
                            reCAPTCHA v3 example
                        </h1>

                      

                        <div class="field">
                            <label class="text">Email</label>
                            <div class="control">
                                <input type="text" name="email" class="input" placeholder="text " >
                            </div>
                        </div>

                        <div class="field is-grouped">
                            <div class="control">
                                <button type="submit" class="button is-link">Send Message</button>
                            </div>
                        </div>

                        <input type="hidden" name="recaptcha_response" id="recaptchaResponse">

                    </form>

                </div>
            </div>
        </div>
    </section>

</body>

</html>